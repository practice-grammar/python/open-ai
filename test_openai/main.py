import openai

from dotenv import load_dotenv
import os

load_dotenv()
print(os.getenv("OPENAI_API_KEY"))
openai.api_key = os.getenv("OPENAI_API_KEY")

prompt = "こんにちは、私の名前は"
model = "text-davinci-002"
response = openai.completions.create(
model=model,
prompt=prompt,
max_tokens=5
)
print(response.choices[0].text)